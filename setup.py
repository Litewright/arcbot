from distutils.core import setup

setup(
    name='arcbot',
    version='1.0',
    packages=['Utilities', 'ArcbotFiles', 'ArcbotFiles.LinuxBot', 'ArcbotFiles.Exceptions', 'ArcbotFiles.RegexHolders',
              'ArcbotFiles.DepreciatedBot', 'ArcbotFiles.MessageHandlers'],
    url='https://Gmandam@bitbucket.org/Gmandam/arcbot.git',
    license='GNU General Public License 3',
    author='Gmandam',
    author_email='lukejunk1234@gmail.copm',
    description='An Easy To Use IRCBot',
    requires=['requests', 'chardet', 'gitdb', 'GitPython']
)
