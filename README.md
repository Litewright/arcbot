# Arcbot #

Arcbot provides a full python 3.4.3 implementation of a chat bot for IRC. Designed primarily to provide archive links on request, it also includes several other useful features such as a last seen feature and a factoids feature as well as several boss level commands to ignore, unignore or otherwise handle your channel as you wish.

The secondary focus of Arcbot is to provide a simple implementation for a persistent IRC chat bot that can be run on any platform. While it works better on a Linux server to ensure constant uptime, the bot itself was primarily developed in a Windows environment and works very effectively within windows. It also includes several features and functions designed to make running the bot as simple as possible such as auto recovery in the event of disconnect, auto ping response and methods to determine if the program itself has crashed due to an unexpected exception, with the ability to restart it. All this combines to make Arcbot incredibly easy to run.

### What is this repository for? ###

* Simple IRC Chat Bot that is designed to be very simple to run.
* Multi Platform. Will run on Windows, Linux or Mac.
* Requires Python 3.4.3. It may run on other versions of 3.X.X but I've not tested it.
* Requires Requests module for Python.
* Version 1.0 of Program.

### Configuration ###

Pull the repository into your folder of choice as so:-

```
#!bash
git pull https://Gmandam@bitbucket.org/Gmandam/arcbot.git origin master

```

Then install the requests module for python using the following command:-

```
#!bash

sudo pip install requests
```

There's some configuration needed before the bot is fully set-up however. Inside ConfigFiles within the git are two configuration files that need to be updated as you want. The first file to worry about is BotConfig.json. This sets out the Host, Port, Nick and various other crucial details behind the bot. 


```
#!python

{
  "HOST": "irc.rizon.net",
  "PORT": 6667,
  "NICK": "Arcbot",
  "IDENT": "ArclightSecretService",
  "REALNAME": "Skynet",
  "PASSWORD": "Z9AyG#cmAteV",
  "EMAIL": " ",
  "PASSCODE": 0,
  "MASTER": " "
}
```


At a minimum, you need to ensure that the Host is for the network you want to run the bot on as well as the Password, Email and Master. The rest can be left alone as you see fit.


```
#!python
[
  "shitbottesting"
]

```


The second file in ConfigFiles to configure is Channels.json. You just need to replace the default channels, the one I use to test the program and the one I actually run the Bot on, with the channels you want to run the bot on. Ideally, you should keep the channels down to two or three popular channels.

```
#!python
[
  "nonservator",
  "Split16",
  "batmanuel"
]

```

The last item to configure can be found in the MemoryFiles area of the Arcbot folder. You need to add yourself to the BossList for the boss level commands to work. You may also wish to add other users to this list as needed.

Once all of this is done, your bot is now ready to run. Simply call PlainPythonIRCBot.py via Python 3.4.3 and let it run. If you use Linux or some form of Linux Bash on PC or Mac, you may wish to place the process in the background or use screen to have it run the process for you.


### Extension and Enhancement ###

Included with the main files are some basic additions that allow you to convert the Python file into a daemon if needed, though significantly less testing has been done on that particular version of the script. You have been warned.

Extending and adding commands is actually pretty easy. Each command is prefaced with a star, *, and a shorthand version of what the command is meant to do. This is seen in the command example below.

```
#!python
 if "*getwatch" in ircmsg:
     # Get the true contents of the message split up as needed
     self.update_command_list(msg[2])

     # Get the true contents of the message split up as needed
     msg_breakup = msg[2].split(" ")

     # Returns the last time the user was seen by the bot, if the user was added to the watch or not.
```

To add your command, simply append it onto PlainPythonIRCBot.py either under the boss commands function or after the msgcheck evaluation on line 959 if it's a general user command. If you intend to use any of the Json files, then you'll have to use the json update and delete functions, simply add the functionality you need and the dict format as shown here

```
#!python
# Update the file according to the schemata dictated by these files
if file_name == self.ignore_reference or file_name == self.boss_list or file_name == self.replies_reference:
    # Get the information from the file
    old_data = file

    # Update the data either using the replies schema or the schema for the other two versions
    if file_name == self.replies_reference:
        old_data[username] = message
    else:
        old_data.append(message)

    # Set the update format
    file = old_data

```

Once done, you can now add the function to run the actual functionality you want to add. I'd recommend including the logging functionality I've added as part of your function just to cover yourself if something goes wrong, otherwise you are golden. Have fun.

### Who do I talk to? ###

Gmandam - lukejunk1234@gmail.com

### Licence ###
Copyright (C) <2016>  <Gmandam>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

More personally, if you do modify the program in any fashion, please send me a link. I may roll the changes into the program.
I also request but don't demand, that you credit me as the one who created the bot.