# Import smtplib for the actual sending function
import smtplib

# Import the email modules we'll need
from email.mime.text import MIMEText


def sendmail(sender, reciever, message, subject="", textfile="", server="localhost", username="", password="", port=0):
    # Create a text/plain message
    if textfile:
        # Open a plain text file for reading.  For this example, assume that
        # the text file contains only ASCII characters.
        fp = open(textfile, 'rb')
        msg = MIMEText(fp.read())
        fp.close()
    elif subject != "":
        textfile = subject
        msg = MIMEText(message)
    else:
        textfile = message
        msg = MIMEText(message)

    # me == the sender's email address
    # you == the recipient's email address
    msg['Subject'] = ('The contents of %s' % textfile)
    msg['From'] = sender
    msg['To'] = reciever

    # Send the message via our own SMTP server, but don't include the
    # envelope header.

    if port != 0:
        s = smtplib.SMTP(server, port)
    else:
        s = smtplib.SMTP(server)

    if server != "localhost":
        s.starttls()
        s.login(username, password)
        s.sendmail(sender, [reciever], msg.as_string())
        s.quit()
    else:
        s.sendmail(sender, [reciever], msg.as_string())
        s.quit()
