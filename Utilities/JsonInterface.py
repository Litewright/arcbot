import json
import numbers


def append_json_record(logger, file_reference, update_data):
    # Get the old information from the list so that we can add new information onto it
    old_data = object()
    try:
        old_data = retrieve_file(logger, file_reference)
        if old_data != update_data:
            raise DataFormatMatchFail
    except FileNotFoundError as err:
        logger.info("Error - %s" % err)
        logger.exception("Exception Traceback")
        old_data = open(file_reference, "w+")
        json.dump(update_data, open(file_reference, "w"), sort_keys=True, separators=(',', ':'), indent=4)
        old_data.close()
    except ValueError:
        json.dump(update_data, open(file_reference, "w"), sort_keys=True, separators=(',', ':'), indent=4)
    except DataFormatMatchFail as err:
        logger.info("Error - %s" % err)
        logger.exception("Exception Traceback")
        raise DataFormatMatchFail

    # Update and remove duplicate items from dict.
    if isinstance(update_data, list):
        old_data.append(update_data)
    elif isinstance(update_data, dict):
        old_data.update(update_data)
    elif isinstance(update_data, numbers.Number) or isinstance(update_data, str):
        old_data += update_data

    # Attempt to dump the updated information back into the file and return it to the person who called the function
    try:
        json.dump(old_data, open(file_reference, "w"), sort_keys=True, separators=(',', ':'), indent=4)
        return old_data
    except KeyError as err:
        logger.info("Error - %s" % err)
        logger.exception("Exception Traceback")
    except IOError as err:
        logger.info("Error - %s" % err)
        logger.exception("Exception Traceback")
    except AttributeError as err:
        logger.info("Error - %s" % err)
        logger.exception("Exception Traceback")


def add_whole_json_record(logger, file_reference, update_data):
    try:
        old_data = open(file_reference, "w+")
        json.dump(update_data, old_data, sort_keys=True, separators=(',', ':'), indent=4)
        old_data.close()
    except KeyError as err:
        logger.info("Error - %s" % err)
        logger.exception("Exception Traceback")
    except IOError as err:
        logger.info("Error - %s" % err)
        logger.exception("Exception Traceback")
    except AttributeError as err:
        logger.info("Error - %s" % err)
        logger.exception("Exception Traceback")


def update_json_list(logger, file_reference, update_key, update_key_two, update_data):
    try:
        return update_json_record(logger, file_reference, [update_key, update_key_two], update_data)
    except Exception as err:
        logger.info("Error - %s" % err)
        logger.exception("Exception Traceback")


def update_json_dict(logger, file_reference, update_key, update_data):
    try:
        return update_json_record(logger, file_reference, update_key, update_data)
    except Exception as err:
        logger.info("Error - %s" % err)
        logger.exception("Exception Traceback")


def update_json_alphanumeric(logger, file_reference, update_key, update_data):
    try:
        return update_json_record(logger, file_reference, update_key, update_data)
    except Exception as err:
        logger.info("Error - %s" % err)
        logger.exception("Exception Traceback")


def update_json_record(logger, file_reference, update_key, update_data):
    old_data = retrieve_file(logger, file_reference)

    # Update and remove duplicate items from dict.
    if isinstance(old_data, list):
        old_data[update_key] = update_data

    elif isinstance(old_data, dict):
        try:
            key_selection = old_data.get(update_key)
        except TypeError:
            key_selection = old_data.get(update_key[0])
        if isinstance(key_selection, list):
            old_data.get(update_key[0])[update_key[1]] = update_data
        elif isinstance(key_selection, dict):
            old_data.get(update_key).update(update_data)
    elif isinstance(old_data, numbers.Number) or isinstance(update_data, str):
        old_data = update_data

    try:
        json.dump(old_data, open(file_reference, "w"), sort_keys=True, separators=(',', ":"), indent=4)
        return old_data
    except KeyError as err:
        logger.info("Error - %s" % err)
        logger.exception("Exception Traceback")
    except IOError as err:
        logger.info("Error - %s" % err)
        logger.exception("Exception Traceback")
    except AttributeError as err:
        logger.info("Error - %s" % err)
        logger.exception("Exception Traceback")


def retrieve_file(logger, file_reference, raw_file=False):
    try:
        with open(file_reference, "r") as file:
            if raw_file:
                data_object = file
                return data_object
            else:
                data_object = json.load(file)
                return data_object
    except KeyError as err:
        logger.info("Error - %s" % err)
        logger.exception("Exception Traceback")
    except IOError as err:
        logger.info("Error - %s" % err)
        logger.exception("Exception Traceback")
    except AttributeError as err:
        logger.info("Error - %s" % err)
        logger.exception("Exception Traceback")


class DataFormatMatchFail(Exception):
    __module__ = 'JsonInterface'

    def __init__(self, arg=""):
        """Raise if the thread remains dead after five attempts"""
        Exception.__init__(self)
        self.msg = arg

    def __str__(self):
        return repr('Data Failed to Update due to Incorrect Data Format' + self.msg)
