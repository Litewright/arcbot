import logging
import logging.handlers
import os


def logger_return(bot_name, file_name):
    log_filename = os.path.join("Logs", file_name)
    logger = logging.getLogger(bot_name)
    logger.setLevel(logging.INFO)
    handler = logging.handlers.RotatingFileHandler(log_filename, encoding='utf8', maxBytes=100000)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    return logger
