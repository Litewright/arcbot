import queue
import threading
import time


class MessageSender(threading.Thread):
    def __init__(self, logger, irc_socket, send_queue, irc_bot, error_queue):
        threading.Thread.__init__(self)
        self.logger = logger
        self.irc_socket = irc_socket
        self.irc_timeout = irc_bot.irc_timeout
        self.irc_buffer_size = irc_bot.irc_buffer_size
        self.send_queue = send_queue
        self.irc_bot = irc_bot
        self.error_queue = error_queue
        self.continue_thread = True
        self.close_event = threading.Event()

    def run(self):
        self.send_messages()

    def join(self, timeout=None):
        self.continue_thread = False
        self.close_event.set()
        threading.Thread.join(self, timeout=2)

    # --------------------------------------------------------------------------------------------------
    # Helper Function

    # Name: Send Message
    # Arguments:    chan = Channel to respond to
    #               msg = Message to be sent
    # Purpose: Respond to user server pings
    #          Send a message to a specific channel

    def send_messages(self):
        while self.continue_thread:
            try:
                irc_msg = self.send_queue.get()
                if irc_msg[2]:
                    self.send_socket_message(irc_msg[0], '#{}'.format(irc_msg[2]))
                else:
                    self.send_socket_message(irc_msg[0], irc_msg[1])
            except queue.Empty:
                pass
            time.sleep(0.00001)

    def send_socket_message(self, irc_msg, chan_user):
        time.sleep(0.5)
        self.irc_socket.send(bytes("PRIVMSG %s :%s \r\n" % (chan_user, irc_msg), "UTF-8"))

    # --------------------------------------------------------------------------------------------------
    # Helper Function

    # Name: restart_connection
    # Arguments:    self = self reference
    # Purpose: Restart the connection if the connection has gone down

    def restart_connection(self):
        self.error_queue.put(1)
        self.continue_thread = False
