import select
import socket
import threading
import time

import chardet


# --------------------------------------------------------------------------------------------------
# Helper Function

# Name: receive_response
# Arguments:    self = self reference
# Purpose: Returns the response from the IRC channel


class MessageProcessor(threading.Thread):
    def __init__(self, logger, irc_socket, messagequeue, irc_bot, error_queue):
        threading.Thread.__init__(self)

        self.logger = logger
        self.irc_socket = irc_socket
        self.irc_timeout = irc_bot.irc_timeout
        self.irc_buffer_size = irc_bot.irc_buffer_size
        self.messagequeue = messagequeue
        self.irc_bot = irc_bot
        self.error_queue = error_queue
        self.continue_run = True
        self.close_event = threading.Event()

    def run(self):
        self.receive_response()

    def join(self, timeout=None):
        self.close_event.set()
        self.continue_run = False
        threading.Thread.join(self, timeout=0.2)

    def receive_response(self):
        preferred_encs = ["UTF-8", "CP1252", "ISO-8859-1"]
        reads = [self.irc_socket]
        self.irc_socket.settimeout(self.irc_timeout)
        self.irc_socket.setblocking(0)
        rep_count = 0
        select_timeout = 60

        while self.continue_run:
            try:
                if self.irc_timeout - rep_count * select_timeout <= 0:
                    raise ConnectionAbortedError
                else:
                    r, w, e = select.select(reads, [], (), select_timeout)
                    if self.irc_socket in r:
                        ircmsg = str
                        try:
                            irc = self.irc_socket.recv(self.irc_buffer_size)
                            for enc in preferred_encs:
                                try:
                                    ircmsg = irc.decode(enc)
                                    break
                                except UnicodeDecodeError:
                                    pass
                            try:
                                ircmsg.encode("UTF-8")
                            except UnicodeDecodeError:
                                try:
                                    enc = chardet.detect(irc)['encoding']
                                    ircmsg = irc.decode(enc, "ignore")
                                except UnicodeDecodeError:
                                    self.logger.info("Chardet Failed, "
                                                     "decoding based off UTF-8 and ignoring all bad bytes.")
                                    ircmsg = irc.decode("UTF-8", "ignore")
                        except socket.error:
                            pass
                        try:
                            if type(ircmsg) is str and len(ircmsg) == 0:
                                raise ConnectionAbortedError
                            elif type(ircmsg is str):
                                self.messagequeue.put(ircmsg)
                            else:
                                self.continue_run = False
                                break
                        except Exception as err:
                            self.logger.info(ircmsg)
                            self.logger.info(err)
                            self.logger.exception("Exception Error")
            except socket.error:
                rep_count += 1
                time.sleep(0.001)
                pass
            except ConnectionAbortedError as err:
                self.logger.info("Error - %s" % err)
                self.logger.info("Connection has reset %s" % self)
                self.logger.exception("Exception Error")
                self.restart_connection()
            except socket.gaierror as err:
                self.logger.info("Error - %s" % err)
                self.logger.info("Connection has reset %s" % self)
                self.logger.exception("Exception Error")
                self.restart_connection()
            except socket.timeout as err:
                self.logger.info("Error - %s" % err)
                self.logger.info("Connection has reset %s" % self)
                self.logger.exception("Exception Error")
                self.restart_connection()
            except OSError as err:
                self.logger.info("Error - %s" % err)
                self.logger.info("Connection has reset %s" % self)
                self.logger.exception("Exception Error")
                self.restart_connection()

    # --------------------------------------------------------------------------------------------------
    # Helper Function

    # Name: restart_connection
    # Arguments:    self = self reference
    # Purpose: Restart the connection if the connection has gone down

    def restart_connection(self):
        print("Shit")
        self.error_queue.put(1)
        self.continue_run = False
