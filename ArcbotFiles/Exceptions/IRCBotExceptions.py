class SystemExitException(Exception):
    __module__ = 'ThreadException'

    def __init__(self, *args):
        """Raise if the thread for the Bot is dead or not."""
        Exception.__init__(self)
        self.args = args

    def __str__(self):
        return ': '.join(self.args)


class DeadThread(Exception):
    __module__ = 'ThreadException'

    def __init__(self, *args):
        """Raise if the thread for the Bot is dead or not."""
        Exception.__init__(self)
        self.args = args

    def __str__(self):
        return ': '.join(self.args)


class NonRestartingThread(Exception):
    __module__ = 'ThreadException'

    def __init__(self, msg=""):
        """Raise if the thread remains dead after five attempts"""
        Exception.__init__(self)
        self.msg = msg


class UpdateException(Exception):
    __module__ = 'ThreadException'

    def __init__(self, *args):
        """Raise if the thread for the Bot is dead or not."""
        Exception.__init__(self)
        self.args = args

    def __str__(self):
        return ': '.join(self.args)
