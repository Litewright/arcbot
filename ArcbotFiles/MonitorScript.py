import json
import logging
import logging.handlers
import os
import sys
import time
from subprocess import call
from threading import Thread

import git

sys.path.append(os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))))
import Utilities.SendEmail as Mail
import ArcbotFiles.Exceptions.IRCBotExceptions as IRCExceptions
import ArcbotFiles.PlainPythonIRCBot as Irc


def start_logger():
    log_filename = os.path.join("Logs", "logging_rotating_file.out")
    gen_logger = logging.getLogger("BotStartLogger")
    gen_logger.setLevel(logging.INFO)
    handler = logging.handlers.RotatingFileHandler(log_filename, encoding='utf8', maxBytes=100000)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    gen_logger.addHandler(handler)
    return gen_logger


def threaded_git_pull():
    git_config = os.path.join("ConfigFiles", "BotConfig.json")

    with open(git_config, "r") as open_file:
        bot_config_list = json.load(open_file)
        path_name = os.path.dirname(os.path.realpath(__file__))
        g = git.cmd.Git(path_name)
        git_url = bot_config_list["GITURL"]
        g.pull(git_url)


def kill_threads(thread_one):
    thread_one.error_queue.join()
    thread_one.message_processor.join()
    thread_one.send_message_processor.join()
    thread_one.irc_socket.shutdown(1)
    thread_one.irc_socket.close()
    thread_one.join()


if __name__ == "__main__":
    os.chdir("..")
    logger = start_logger()
    t1 = Irc.IrcBot(logger)
    t1.start()

    count = 0

    while True:
        try:
            count = 0
            if t1.kill:
                raise IRCExceptions.SystemExitException
            if not t1.is_alive():
                raise IRCExceptions.DeadThread
            if t1.update:
                raise IRCExceptions.UpdateException
        except IRCExceptions.DeadThread:
            try:
                if count >= 5:
                    time.sleep(10)
                    t1 = Irc.IrcBot(logger)
                    t1.start()
                    time.sleep(10)
                    obj = t1.is_alive()
                    if t1.kill:
                        raise IRCExceptions.NonRestartingThread
                logger.info("Error - Restarting Thread - Thread died for some reason.")
                kill_threads(t1)

                count += 1
                time.sleep(20)
            except IRCExceptions.NonRestartingThread:
                call("echo ""Thread Unable to Restart, "
                     "Check IRC Bot"" | Mail -s ""IRC Bot Failure"" Gmandam@mykolab.com")
                Mail.sendmail("ArcbotDistressSystem@TheBot.com", "Gmandam@mykolab.com",
                              message="The Bot Has Failed. Jesus Christ")
                logger.error("Thread Will Not Restart")
                sys.exit("1")
        except IRCExceptions.UpdateException:
            thread = Thread(target=threaded_git_pull())
            thread.start()
            thread.join()
            t1.error_queue.join()
            kill_threads(t1)
            time.sleep(5)
            t1 = Irc.IrcBot(logger)
            t1.start()
            logger.info("Bot Restarted - Ready To Continue")
        except IRCExceptions.SystemExitException:
            kill_threads(t1)
            logger.info("Bot Killed By Command")
            sys.exit(1)
        time.sleep(1)
