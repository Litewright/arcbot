import json
import logging
import logging.handlers
import os
import queue
import random
import re
import socket
import sys
import threading
import time
from queue import Queue

import requests
import requests.structures

sys.path.append(os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))))
import ArcbotFiles.RegexHolders.RegexHolder as UrlMarker
import ArcbotFiles.HelperFunctions.HelperFunctions as HelperFunctions
from ArcbotFiles.MessageHandlers.MessageProcessor import MessageProcessor
from ArcbotFiles.MessageHandlers.MessageSender import MessageSender


class IrcBot(threading.Thread):
    def __init__(self, error_logger=None):
        threading.Thread.__init__(self)
        # IRC socket and Command List
        self.irc_socket = socket.socket()

        # IRC socket timeout
        # Set to at least server ping time plus 10-15 seconds
        self.irc_timeout = 240.0
        self.irc_socket.settimeout(self.irc_timeout)

        # IRC message input buffer size
        self.irc_buffer_size = 4096
        self.command_list = list()

        # File References
        self.last_seen_reference = os.path.join("MemoryFiles", "LastSeen.json")
        self.factoid_reference = os.path.join("MemoryFiles", "Factoids.json")
        self.boss_reference = os.path.join("MemoryFiles", "BossList.json")
        self.ignore_reference = os.path.join("MemoryFiles", "IgnoreList.json")
        self.replies_reference = os.path.join("MemoryFiles", "Replies.json")
        self.help_reference = os.path.join("Help", "Help.txt")
        self.channel_reference = os.path.join("ConfigFiles", "Channels.json")
        self.log_filename = os.path.join("Logs", "logging_rotating_file.out")
        self.bot_config = os.path.join("ConfigFiles", "BotConfig.json")

        # IRC Server Settings
        bot_config_list = json.load(open(self.bot_config, "r"))
        self.HOST = bot_config_list["HOST"]
        self.PORT = bot_config_list["PORT"]

        # Bot Name and Configuration Settings
        self.NICK = bot_config_list["NICK"]
        self.IDENT = bot_config_list["IDENT"]
        self.REALNAME = bot_config_list["REALNAME"]
        self.REGPASSWORD = bot_config_list["PASSWORD"]
        self.EMAIL = bot_config_list["EMAIL"]
        self.PASSCODE = bot_config_list["PASSCODE"]
        # Names the bot owner
        self.MASTER = bot_config_list["MASTER"]
        self.CHANNEL = json.load(open(self.channel_reference, "r"))

        # Gives the help list responses
        self.response_array = list(open(self.help_reference, "r"))

        # Setup access to the ignore list and the boss list
        self.ignore_list = self.update_ignore_list()
        self.boss_list = json.load(open(self.boss_reference, "r"))

        # Various inter-thread queues for communicating messages to be sent, processed and the current status of all
        # threads
        self.message_queue = Queue()
        self.send_message_queue = Queue()
        self.error_queue = Queue()

        # The actual thread processors. These handle messages coming into the bot and messages coming out of the bot
        self.message_processor = threading.Thread
        self.send_message_processor = threading.Thread

        # Potential commands within the program
        self.user_functions = {
            "*return": self.chan_or_private_message,
            "*rejoin": self.check_channels,
            "*arc": self.archive_site,
            "*lastmsg": self.print_last_commands,
            "*addfact": self.update_json_file,
            "*delfact": self.remove_json_list,
            "*printfact": self.print_factoids,
            "*help": self.print_help_information,
            "*addwatch": self.last_seen,
            "*getwatch": self.chan_or_private_message,
            "*delwatch": self.remove_json_list
        }

        self.boss_functions = {
            "*ignore": self.add_ignore_list,
            "*delignore": self.delete_ignore_list,
            "*registerbot": self.register_nickname,
            "*starttrigger": self.start_end_triggers,
            "*disabletrigger": self.start_end_triggers,
            "*addboss": self.add_boss,
            "*delboss": self.del_boss,
            "*kill": self.kill_bot,
            "*sleep": self.sleep_or_awake,
            "*awake": self.sleep_or_awake,
            "*update": self.update_bot,
            "*joinchan": self.join_channels,
            "*leavechan": self.leave_channels
        }

        # Setup the logging to evaluate when things go wrong
        if not error_logger:
            self.logger = logging.getLogger("BotLogger")
            self.logger.setLevel(logging.INFO)
            self.handler = logging.handlers.RotatingFileHandler(self.log_filename, encoding='utf8', maxBytes=100000)
            self.formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            self.handler.setFormatter(self.formatter)
            self.logger.addHandler(self.handler)
        else:
            self.logger = error_logger

        # Setup the amusing replies list
        self.replies = requests.structures.CaseInsensitiveDict()
        self.replies = json.load(open(self.replies_reference, "r"))

        # Various states the bot can be in. Ranging from needing to be updated, to being dead, to currently running,
        # to being asleep
        self.kill = False
        self.sleep = False
        self.update = False
        self.running = True

        # Denotes the close event for when the bot is shut down
        self.close_event = threading.Event()

    def run(self):
        self.start_bot()

    def join(self, timeout=None):
        self.close_event.set()
        self.running = False
        threading.Thread.join(self, timeout=0.2)

    def error_queue_check(self):
        try:
            task = self.error_queue.get(timeout=2)
            if task == 1:
                self.error_queue.join()
                self.message_processor.join(self)
                self.send_message_processor.join(self)
                self.restart_connection()
        except queue.Empty:
            pass

    # --------------------------------------------------------------------------------------------------
    # Helper Function

    # Name: Ping
    # Arguments: None
    # Purpose: Respond to server pings.

    def ping(self, msg=''):
        self.irc_socket.send(bytes("PONG %s \r\n" % (msg), "UTF-8"))

    # --------------------------------------------------------------------------------------------------
    # Helper Function

    # Name: send_command
    # Arguments:    cmd_server = The specific service serving the command
    #               cmd_nature_1 = The first command part to be served
    #               cmd_nature_2 = A second optional command part to be served
    #               cmd_nature_3 = A third optional command part to be served
    #               cmd_nature_4 = A fourth optional command part to be served
    # Purpose: To send commands to the IRC Server as needed

    def send_command(self, cmd_server, cmd_nature_1, cmd_nature_2="", cmd_nature_3="", cmd_nature_4=""):
        send_msg = ("%s %s %s %s %s" %
                    (cmd_server, cmd_nature_1, cmd_nature_2, cmd_nature_3, cmd_nature_4)).strip() + "\r\n"
        self.irc_socket.send(bytes(send_msg, "UTF-8"))
        # self.send_message_queue.put(send_msg)

    # --------------------------------------------------------------------------------------------------
    # Helper Function

    # Name: Join Channel
    # Arguments:    channel_list = channel
    # Purpose: Join a channel

    def join_chan(self, channel_list):
        for chan in channel_list:
            self.irc_socket.send(bytes("JOIN #%s \r\n" % chan, "UTF-8"))

    # --------------------------------------------------------------------------------------------------
    # Helper Function

    # Name: Leave Channel
    # Arguments:    channel_list = channel
    # Purpose: Leave a channel

    def leave_chan(self, channel_list):
        for chan in channel_list:
            self.irc_socket.send(bytes("PART #%s \r\n" % chan, "UTF-8"))

    # --------------------------------------------------------------------------------------------------
    # Helper Function

    # Name: filter_command
    # Arguments:    ircmsg = message received
    #               username = username of the person that issued the command
    # Returns:  msg = command the user sent
    # Purpose: Filter the command that was received

    def filter_command(self, ircmsg, username, sleep=False):
        # Returns the contents of the message that triggered the command
        temp_msg = ircmsg.split('PRIVMSG')
        if len(temp_msg) >= 2 and 'PRIVMSG' in ircmsg:
            msg = temp_msg[1].replace("\r\n", "").split(":", 1)[1]
        else:
            pattern_match = re.findall("PING\s:\d+", ircmsg)
            if len(pattern_match) > 0:
                return re.findall("[\d]+", pattern_match[0])[0], False
            else:
                return "", False

        msg_check = False
        try:
            var_truth1 = msg.startswith("*")
            var_truth2 = username[0] not in self.ignore_list
            if (username in self.boss_list or not sleep) and (var_truth1 and var_truth2):
                msg_check = True
        except NameError:
            pass
        except Exception:
            pass

        return msg, msg_check

    # --------------------------------------------------------------------------------------------------
    # Helper Function

    # Name: set_username
    # Arguments:    nick = Nickname
    # Purpose: Set the bot nickname

    def set_username(self, nick):
        # Assign the nickname as given
        HelperFunctions.send_command(self.irc_socket, "NICK", nick)

        # Assign user identity, host and real name
        HelperFunctions.send_command(self.irc_socket, "USER", self.IDENT, self.HOST, "bla :", self.REALNAME)

        # Identify the bot as the correct owner of the name
        HelperFunctions.send_command(self.irc_socket, "NICKSERV", "IDENTIFY", self.REGPASSWORD)

        # Actually check if the bot is registered or not
        recv_return = HelperFunctions.get_queue_messages(self.message_queue, -1)

        # If the bot isn't registered, begin the steps to register the bot
        if "Your nick isn't registered" in recv_return:
            self.register_nickname(nick)

    # --------------------------------------------------------------------------------------------------
    # Helper Function

    # Name: restart_connection
    # Arguments:    self = self reference
    # Purpose: Restart the connection if the connection has gone down

    def restart_connection(self):
        self.logger.info("Connection Dropped. Beginning Reconnection Cycle")
        time.sleep(15)
        self.irc_socket.shutdown(1)
        self.irc_socket.close()
        self.logger.info("Sockets Closed. Waiting for System to Register.")
        time.sleep(15)
        self.logger.info("Connections cleaned, restarting bot.")
        self.start_bot()

    # --------------------------------------------------------------------------------------------------
    # Helper Function

    # Name: register_nickname
    # Arguments:    nick = Nickname
    # Purpose: Register with nickserv if a nickserv exists

    def register_nickname(self, msg_contents, nick=""):
        try:
            self.PASSCODE = int(msg_contents.strip())
        except ValueError:
            self.PASSCODE = 0
        if nick:
            self.set_username(nick)
            self.NICK = nick

        HelperFunctions.send_command(self.irc_socket, "NICKSERV", "REGISTER", self.REGPASSWORD, self.EMAIL)

        if self.PASSCODE == 0:
            self.chan_or_private_message(self.MASTER, "Bot has sent request for registration, please read your email.")
        else:
            HelperFunctions.send_command(self.irc_socket, "NICKSERV", "CONFIRM", str(self.PASSCODE))

    # --------------------------------------------------------------------------------------------------
    # Helper Function

    # Name: chan_or_private_message
    # Arguments:    chan = possible channel
    #               user = possible user
    # Purpose: Determine if the bot should respond to a private message or a public irc channel

    def chan_or_private_message(self, user, msg, chan=""):
        varsval = [msg, user, chan]
        self.send_message_queue.put(varsval)
        # for var in varsval:
        #     self.send_message_queue.put(var)

    # --------------------------------------------------------------------------------------------------
    # Helper Function

    # Name: update_ignore_list
    # Arguments:    self = self reference
    # Purpose: Updates the ignore list to account for changes

    def update_ignore_list(self):
        temp_list = HelperFunctions.retrieve_json(self.ignore_reference)
        self.ignore_list = temp_list
        return temp_list

    # --------------------------------------------------------------------------------------------------
    # Helper Function

    # Name: check_channels
    # Arguments:
    # Returns:  is_connected = true or false value
    #           channel_list = List of currently connected channels
    # Purpose:  To check if the bot is connected to the channels it should be connected to

    def check_channels(self, subject_username, expected_channel_list):
        HelperFunctions.send_command(self.irc_socket, "WHOIS", subject_username)
        whois_response = ""

        # Until we receive either a privmessage or a End of WHOIS message, keep receiving data from the server
        while not (("End of /WHOIS list." in whois_response) or ("PRIVMSG" in whois_response)):
            whois_response = HelperFunctions.get_queue_messages(self.message_queue) + whois_response

        try:
            whois = whois_response.split("%s %s" % (self.NICK, self.NICK), 1)[1]
        except IndexError:
            whois = whois_response

        channel_list = HelperFunctions.filter_channel(whois)
        channel_list = [channel.replace("#", '') for channel in channel_list]
        try:
            if bool(len(channel_list) == len(expected_channel_list)):
                return True
            else:
                return False
        except ValueError:
            return False

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------

    # --------------------------------------------------------------------------------------------------
    # Subroutine

    # Name: archive_website
    # Arguments:    msg = channel message
    #               channel = channel sender
    # Purpose: Respond with archived website

    def archive_site(self, msg, user, channel=""):
        archive_website = "http://archive.is/submit/"
        websites = re.findall(UrlMarker.URL_REGEX, msg)

        for (index, website) in enumerate(websites):
            try:

                r = requests.post(archive_website, data={"url": website})
                string = str(r.headers)
                new_addresses = [item for item in re.findall(UrlMarker.URL_REGEX, string) if "archive.is" != item]
                send_message = user + ": "

                # Evaluate if the page has already been archived or not. Change the behaviour depending on that
                try:
                    send_url = r.history[0].headers["Location"]
                    send_message = user + ": website has already been archived at - "
                except IndexError:
                    send_url = new_addresses[len(new_addresses) - 1]

                # Send the message to the user or the channel, depending on how many links were requested
                self.chan_or_private_message(user, send_message + send_url, channel)

            except Exception as err:
                self.chan_or_private_message(user, "Server Error Response. Try again later.", channel)
                HelperFunctions.log_basic_info(self, self.logger, err)
                break

    # --------------------------------------------------------------------------------------------------
    # Subroutine

    # Name: update_command_list
    # Arguments:    msg = channel message
    # Purpose: Update last five issued commands list as needed

    def update_command_list(self, msg):
        if len(self.command_list) >= 5:
            self.command_list.pop(0)
            self.command_list.append(msg)
        else:
            self.command_list.append(msg)

    # --------------------------------------------------------------------------------------------------
    # Subroutine

    # Name: print_last_commands
    # Arguments:    msg = channel message
    #               sender = message sender
    # Purpose: Respond with archived website

    def print_last_commands(self, user):

        for (index, line) in enumerate(self.command_list):
            self.chan_or_private_message(user, "Last Command No. %s was %s" % (index + 1, line))

    # --------------------------------------------------------------------------------------------------
    # Subroutine

    # Name: remove_json_list
    # Arguments:    file_name = json file name reference
    #               username = account being tracked or removed from
    #               message = details being edited
    # Purpose: Delete the selected information from the json file.

    def remove_json_list(self, file_name, username, message=""):
        try:
            # Open up the json file that you wish to update
            with open(file_name, "r") as json_file:
                file = json.load(json_file)

                # Update the file according to the schemata dictated by these files
                if file_name == self.ignore_reference or file_name == self.boss_list:
                    # Update the new information with a list comprehension designed to filter out the specific element
                    # I am looking to delete.
                    new_data = [x for x in file if not x == message]

                # Update the file according to the schemata dictated by this file
                elif file_name == self.factoid_reference:

                    # # Get the specific list we are looking to edit
                    #
                    # # Pop the element from the list
                    # file[username].pop(int(message) - 1)
                    #
                    # # New data is updated according to the format dictated
                    # new_data = {username: old_data}
                    #
                    # # Update the file with the new information
                    # file.update(new_data)
                    file[username].pop(int(message) - 1)
                    new_data = file

                elif file_name == self.last_seen_reference:
                    # Get the specific list we are looking to edit
                    file.pop(message)
                    new_data = file

                # Otherwise provide a default schemata for deletions from our files
                else:
                    if isinstance(file, dict):
                        new_data = list()
                        file.update(new_data)
                        new_data = {username: file}
                    else:
                        new_data = [x for x in file if not x == message]
            with open(file_name, "w") as file:
                json.dump(new_data, file, separators=(',', ':'), indent=4)

        except KeyError as err:
            HelperFunctions.log_basic_info(self, self.logger, err)

        except IOError as err:
            HelperFunctions.log_basic_info(self, self.logger, err)

        except AttributeError as err:
            HelperFunctions.log_basic_info(self, self.logger, err)

    # --------------------------------------------------------------------------------------------------
    # Subroutine

    # Name: update_json_file
    # Arguments:    file_name = json file name reference
    #               username = account being tracked or added to
    #               message = details being edited
    # Purpose: Update the json file selected with new information

    def update_json_file(self, file_name, username, message=""):
        try:
            # Open up the json file that you wish to update
            with open(file_name, "r") as file_json:
                file = json.load(file_json)

                # Update the file according to the schemata dictated by these files
                if file_name == self.ignore_reference or file_name == self.boss_list or file_name == self.replies_reference:
                    # Get the information from the file
                    old_data = file

                    # Update the data either using the replies schema or the schema for the other two versions
                    if file_name == self.replies_reference:
                        old_data[username] = message
                    else:
                        if message not in old_data:
                            old_data.append(message)

                    # Set the update format
                    file = old_data

                # Update the file according to the schemata dictated by these files
                elif file_name == self.factoid_reference:
                    # Get the information from the file
                    try:
                        old_data = file[username]

                        # Update the information
                        old_data.append(message)

                        # Set the update format
                        data_format = {username: old_data}

                        # Update the original file for dumping back into the json file
                        file.update(data_format)
                    except KeyError:
                        old_data = list()
                        # Update the information
                        old_data.append(message)

                        # Set the update format
                        data_format = {username: old_data}

                        # Update the original file for dumping back into the json file
                        file.update(data_format)

                # Update the file according to the schemata dictated by these files
                elif file_name == self.last_seen_reference:
                    # Get the information from the file
                    try:
                        # Update the information
                        old_data = list()
                        old_data.append(message)

                        # Set the update format
                        data_format = {username: old_data}

                        # Update the original file for dumping back into the json file
                        file.update(data_format)
                    except KeyError:
                        # Update the information
                        old_data = list()
                        old_data.append(message)

                        # Set the update format
                        data_format = {username: old_data}

                        # Update the original file for dumping back into the json file
                        file.update(data_format)

                # Provide a default schemata if needed
                else:
                    # Get the information from the file
                    if isinstance(file, dict):
                        old_data = file[username]
                        data_format = {username: old_data}
                        file.update(data_format)
                    else:
                        if message not in file:
                            file.append(message)
            # Dump the information back into the file
            with open(file_name, "w") as file_json:
                json.dump(file, file_json, separators=(',', ':'), indent=4)
        except KeyError as err:
            # Evaluate if the error is due to a key error or not.
            if err.args[0] == username:
                try:
                    with open(file_name, "r") as file_json:
                        file = json.load(file_json)
                        # Setup the format for the factoids to match the factoids already stored on file.
                        data_format = {username: message}
                        # Update the file and dump the data to the json file.
                        file.update(data_format)
                    with open(file_name, "w") as file_json:
                        json.dump(file, file_json, separators=(',', ':'), indent=4)
                except KeyError as err:
                    HelperFunctions.log_basic_info(self, self.logger, err)

                except IOError as err:
                    HelperFunctions.log_basic_info(self, self.logger, err)

                except AttributeError as err:
                    HelperFunctions.log_basic_info(self, self.logger, err)

        except IOError as err:
            HelperFunctions.log_basic_info(self, self.logger, err)

        except AttributeError as err:
            HelperFunctions.log_basic_info(self, self.logger, err)

    # --------------------------------------------------------------------------------------------------
    # Subroutine

    # Name: print_factoids
    # Arguments:    file_name = json file name reference
    #               storage_data = the storage data containing the account name and the actual value
    #               channel = The channel where the request was made
    #               username = The private message where the request was made
    # Purpose: Print a factoid from the JSON file

    def print_factoids(self, storage_data, channel="", username=""):
        storage_name = storage_data[1].strip().strip()
        storage_number = 0
        try:
            try:
                storage_number = storage_data[2]
            except IndexError:
                pass
            # Open up the specified JSON file and check if the username is present or not
            file = HelperFunctions.retrieve_json(self.factoid_reference)
            old_data = file[storage_name]

            # If the number is not set to random, check to see if the factoid number exists or not/ is within the list
            # bounds
            try:
                # Check if the user wanted all facts to be printed about a user
                try:
                    factoid = old_data[int(storage_number) - 1]
                except ValueError:
                    if storage_number == "All":
                        factoid = random.choice(old_data) + " " + random.choice(old_data)
                        factoid += "\n There are an additional " + str(len(old_data) - 3) + " facts for this user."
                    # Select a random fact to be given about the user
                    else:
                        factoid = old_data[random.randint(0, len(old_data) - 1)]

                self.chan_or_private_message(username, storage_name + ": " + factoid + ".", channel)
            except IndexError as err:
                self.chan_or_private_message(username, "No Factoid associated with "
                                                       "that number or number not added.", channel)
                HelperFunctions.log_basic_info(self, self.logger, err)

        except ValueError as err:
            # Provide a check to see if the user attempted to enter a number below 1
            self.chan_or_private_message(username, "Value is below 1. Please enter a number equal to or greater than 1",
                                         channel)
            HelperFunctions.log_basic_info(self, self.logger, err)

        except KeyError as err:
            if err.args[0] == storage_name:
                self.chan_or_private_message(username, "No Factoids Associated with User.", channel)
            else:
                HelperFunctions.log_basic_info(self, self.logger, err)

    # --------------------------------------------------------------------------------------------------
    # Subroutine

    # Name: last_seen
    # Arguments:    requesting_username = User who made the request
    #               channel = Channel where the request was made
    #               subject_username = user message sent
    # Purpose: Add a user to be watched for the last time they were seen.

    def last_seen(self, requesting_username, channel, subject_username):

        # Sends a WHOWAS request to the server in order to determine if the user is online or not
        HelperFunctions.send_command(self.irc_socket, "WHOWAS", subject_username)

        # Sets up a reciever for the response for the WHOWAS message
        whowas_response = ""

        # Until we receive either a privmessage or a End of WHOWAS message, keep receiving data from the server
        while not ("End of WHOWAS" in whowas_response) or ("PRIVMSG" in whowas_response):
            try:
                whowas_response = HelperFunctions.get_queue_messages(self.message_queue) + whowas_response
                whowas_response = whowas_response.strip("\n\r")
            except queue.Empty:
                return ""
                pass

        # Check the WHOIS response to see if they are online or not
        HelperFunctions.send_command(self.irc_socket, "WHOIS", subject_username)

        # Sets up a reciever for the response for the WHOIS message
        whois_response = ""

        # Until we receive either a privmessage or a End of WHOIS message, keep receiving data from the server
        while not ("End of /WHOIS list." in whois_response) or ("PRIVMSG" in whois_response):
            try:
                whois_response = HelperFunctions.get_queue_messages(self.message_queue) + whois_response
            except queue.Empty:
                return ""
                pass

        # Filter the ircmsg response for a specific format. This format is the date and time of when the user was last
        # online
        ircmsg_contents = re.findall(UrlMarker.DATE_TIME_REGEX, whowas_response)
        whois_contents = whois_response.split("\r\n")

        # Check if the WHOIS response came back with information regarding if the user was online or not.
        # If it"s greater than two, then the user is online, else the user is offline.
        if len(whois_contents) > 2:
            # Send a message to the user that the user they wish to watch is online
            self.chan_or_private_message(requesting_username, "User is Online!", channel)

            # Format the current time for the json file
            time_date = time.strftime("%a %b %d %X %Y")

            # Format the message for the json file.
            respon_msg = "Last seen at " + str(time_date)

            # Update the associated file with the information
            self.update_json_file(self.last_seen_reference, subject_username, respon_msg)
        else:
            if len(ircmsg_contents) > 2:
                # Format the message for the json file.
                # Last time seen on the network is set by the last element in the list
                respon_msg = ircmsg_contents[len(ircmsg_contents) - 2]

                # Update the associated file with the information
                self.update_json_file(self.last_seen_reference, subject_username, respon_msg)
            else:
                respon_msg = "Last seen unknown"
                self.update_json_file(self.last_seen_reference, subject_username, respon_msg)

    # --------------------------------------------------------------------------------------------------
    # Subroutine

    # Name: print_help_information
    # Arguments:    username = User who made the request
    #               channel = Channel where the request was made
    # Purpose: Print help information for the user to have

    def print_help_information(self, username):
        temp = "Total number of active commands is {0} : - ".format(str(round(len(self.response_array) / 2 - 1)))
        self.chan_or_private_message(username, temp)

        for (index, response) in enumerate(self.response_array):
            self.chan_or_private_message(username, response)

    # --------------------------------------------------------------------------------------------------
    # Subroutine

    # Name: initial_bot_response
    # Argument:     self = self reference
    # Returns:      True/False depending on execution status
    # Purpose: Initial bot setup

    def initial_bot_response(self):
        # Connect to the IRC server via a socket
        try:
            self.irc_socket.settimeout(self.irc_timeout)
            self.irc_socket = socket.socket()
            self.irc_socket.connect((self.HOST, self.PORT))

            self.message_processor = MessageProcessor(self.logger, self.irc_socket,
                                                      self.message_queue, self, self.error_queue)
            self.send_message_processor = MessageSender(self.logger, self.irc_socket,
                                                        self.send_message_queue, self, self.error_queue)
            self.message_processor.start()
            self.send_message_processor.start()

            # Assign self username, identity, host and real name
            self.set_username(self.NICK)
            irc_msg = HelperFunctions.get_queue_messages(self.message_queue)
            while "ping" not in irc_msg.lower():
                irc_msg = HelperFunctions.get_queue_messages(self.message_queue)

            username = HelperFunctions.filter_username(irc_msg)
            msg, msg_check = self.filter_command(irc_msg, username, sleep=self.sleep)
            if msg:
                self.ping(msg)
                # Message bot owner to say the bot is online
                time.sleep(2)
                self.chan_or_private_message(self.MASTER, "Hello Master\r\n")
                self.join_chan(self.CHANNEL)

            self.logger.info("Bot Has Started")
        except ConnectionAbortedError as err:
            HelperFunctions.log_basic_info(self, self.logger, err)
            self.restart_connection()
        except socket.gaierror as err:
            HelperFunctions.log_basic_info(self, self.logger, err)
            self.restart_connection()
        except socket.timeout as err:
            HelperFunctions.log_basic_info(self, self.logger, err)
            self.restart_connection()
        except OSError as err:
            HelperFunctions.log_basic_info(self, self.logger, err)
            self.restart_connection()

    # --------------------------------------------------------------------------------------------------
    # Subroutine

    # Name: amusing_retorts
    # Arguments:    msg = The actual contents of what the user requested
    #               username = The username/s of the user making the command request
    #               channel = The channel where the request came from
    # Purpose: Responding in an amusing fashion to insults from filthy humans

    def amusing_retorts(self, username, channel, msg):

        msg_breakup = msg.split(" ")

        for word in msg_breakup:
            if word in self.replies and username != "" and username != self.NICK and not msg.startswith("*") \
                    and self.NICK in msg and not self.replies[word].startswith("#"):
                temp = self.replies[word]
                self.chan_or_private_message(username, temp, channel)

    # --------------------------------------------------------------------------------------------------
    # Subroutine

    # Name: join_channels
    # Arguments: msg = The actual contents of what of the user requested
    # Purpose: Allowing the user to set the channels the bot can join

    def join_channels(self, msg):
        if msg:
            channels = msg
            channels.pop(0)

            channel_list = list()

            for channel in channels:
                channel_list.append(channel.replace("#", ""))

            self.join_chan(channel_list)

            channel_list.append(self.CHANNEL)
            try:
                json.dump(self.CHANNEL, open(self.channel_reference, "w"), separators=(',', ':'), indent=4)
            except IOError as err:
                HelperFunctions.log_basic_info(self, self.logger, err)

    # --------------------------------------------------------------------------------------------------
    # Subroutine

    # Name: leave_channels
    # Arguments: msg = The actual contents of what of the user requested
    # Purpose: Allowing the user to set the channels the bot will leave

    def leave_channels(self, msg):
        if msg:
            if isinstance(msg, str):
                channels = msg.split(" ")
            else:
                channels = msg
            channels.pop(0)

            channel_list = list()

            for channel in channels:
                channel_list.append(channel.replace("#", ""))

            self.leave_chan(channel_list)

            new_channel_list = [chan for chan in self.CHANNEL if chan not in channel_list]
            self.CHANNEL = new_channel_list

            try:
                with open(self.channel_reference, "w") as file:
                    json.dump(new_channel_list, file, separators=(',', ':'), indent=4)
            except IOError as err:
                HelperFunctions.log_basic_info(self, self.logger, err)

    def add_ignore_list(self, username, msg_contents):
        if msg_contents:
            self.update_json_file(self.ignore_reference, username, msg_contents.strip().strip())
            self.update_ignore_list()

    def delete_ignore_list(self, username, msg_contents):
        if msg_contents:
            self.remove_json_list(self.ignore_reference, username, msg_contents.strip().strip())
            self.update_ignore_list()

    def start_end_triggers(self, msg_contents, start_trigger=True):
        if msg_contents:
            if self.replies[msg_contents].startswith("#") and start_trigger:
                self.replies[msg_contents] = self.replies[msg_contents].strip("#")
                self.update_json_file(self.replies_reference, msg_contents, self.replies[msg_contents])
            elif not self.replies[msg_contents].startswith("#"):
                self.replies[msg_contents] = "# " + self.replies[msg_contents]
                self.update_json_file(self.replies_reference, msg_contents, self.replies[msg_contents])

    def add_boss(self, msg_contents, username):
        if msg_contents:
            if msg_contents not in self.boss_list:
                self.boss_list.append(msg_contents)
            self.update_json_file(self.boss_reference, username, msg_contents)

    def del_boss(self, msg_contents, username):
        if msg_contents:
            self.boss_list = [x for x in self.boss_list if not x == msg_contents]
            self.remove_json_list(self.boss_reference, username, msg_contents)

    def kill_bot(self, username, channel):
        self.chan_or_private_message(username, "Turning Off Now.", channel)
        self.kill = True
        sys.exit(0)

    def sleep_or_awake(self, username, channel):
        self.sleep = not self.sleep
        if self.sleep:
            self.chan_or_private_message(username, "Sleep time. Night Night.", channel)
        else:
            self.chan_or_private_message(username, "It's time to get up. The day is beginning.", channel)

    def update_bot(self, username, channel):
        self.logger.info("Bot Restarting - Ready For Update")
        self.chan_or_private_message(username, "Bot is restarting. See you soon :)", channel)
        self.update = True

    def boss_arguments(self, username, channel, msg_breakup):
        msg_command = msg_breakup[0]
        try:
            msg_contents = msg_breakup[1].strip().strip()
        except IndexError:
            msg_contents = ""

        try:
            if len(msg_breakup) > 2:
                channel_content = " ".join((msg_breakup[2:len(msg_breakup)]))
            else:
                channel_content = ["", msg_breakup[1]]
        except IndexError:
            channel_content = ""
        executed_arguments = {
            "*ignore": {"username": username, "msg_contents": msg_contents},
            "*delignore": {"username": username, "msg_contents": msg_contents},
            "*registerbot": {"msg_contents": msg_contents, "nick": self.NICK},
            "*starttrigger": {"msg_contents": msg_contents, "start_trigger": True},
            "*disabletrigger": {"msg_contents": msg_contents, "start_trigger": False},
            "*addboss": {"msg_contents": msg_contents, "username": username},
            "*delboss": {"msg_contents": msg_contents, "username": username},
            "*kill": {"username": username, "channel": channel},
            "*sleep": {"username": username, "channel": channel},
            "*awake": {"username": username, "channel": channel},
            "*update": {"username": username, "channel": channel},
            "*joinchan": {"msg": channel_content},
            "*leavechan": {"msg": channel_content}
        }
        return executed_arguments.get(msg_command)

    def return_boss_functions(self, msg_breakup):
        return self.boss_functions.get(msg_breakup[0])

    def user_arguments(self, username, channel, msg, msg_breakup):

        msg_command = msg_breakup[0]
        try:
            msg_contents = msg_breakup[1].strip().strip()
        except IndexError:
            msg_contents = ""

        try:
            factoid_content = " ".join((msg_breakup[2:len(msg_breakup)]))
        except IndexError:
            factoid_content = ""

        try:
            watch_contents = str(HelperFunctions.retrieve_json(self.last_seen_reference)[msg_contents][0])
        except KeyError:
            watch_contents = ""

        executed_arguments = {
            "*return": {"user": username, "msg": "Return String Test", "chan": channel},
            "*rejoin": {"subject_username": self.NICK, "expected_channel_list": self.CHANNEL},
            "*arc": {"msg": msg, "user": username, "channel": channel},
            "*lastmsg": {"user": username},
            "*addfact": {"file_name": self.factoid_reference, "username": msg_contents, "message": factoid_content},
            "*delfact": {"file_name": self.factoid_reference, "username": msg_contents, "message": factoid_content},
            "*printfact": {"storage_data": msg_breakup, "channel": channel, "username": username},
            "*help": {"username": username},
            "*addwatch": {"requesting_username": username, "channel": channel, "subject_username": msg_contents},
            "*getwatch": {"user": username, "msg": username + ", User requested was last seen at: - " + watch_contents,
                          "chan": channel},
            "*delwatch": {"file_name": self.last_seen_reference, "username": username, "message": msg_contents}
        }
        return executed_arguments.get(msg_command)

    def return_user_functions(self, msg_breakup):
        return self.user_functions.get(msg_breakup[0])

    # --------------------------------------------------------------------------------------------------
    # Subroutine

    # Name: start_bot
    # Arguments:    self = self reference
    # Purpose: Start and command the bot

    def start_bot(self):

        self.initial_bot_response()

        # Keep bot active as long as possible.
        while self.running:
            self.error_queue_check()
            try:
                irc_msg = HelperFunctions.get_queue_messages(self.message_queue)
                if type(irc_msg) is not str:
                    continue
                channel = HelperFunctions.filter_channel(irc_msg)
                username = HelperFunctions.filter_username(irc_msg)
                msg, msg_check = self.filter_command(irc_msg, username, sleep=self.sleep)
                msg_breakup = msg.split(" ")

                # Respond to a ping from the server
                if "PING" in irc_msg.upper() and msg:
                    self.ping(msg)
                elif "PING" in irc_msg.upper():
                    self.ping()
                # Amusing retorts from the bot
                if "PRIVMSG" in irc_msg and username[0] != "" and not ((len(channel) > 1) or (len(username) > 1)):
                    self.amusing_retorts(username[0], channel[0], msg)

                try:
                    used = False
                    # Check if the message received from the IRC server was to execute a boss command or not
                    if username[0] in self.boss_list:
                        variable_dict = self.boss_arguments(username[0], channel[0], msg_breakup)
                        if variable_dict:
                            self.return_boss_functions(msg_breakup)(**variable_dict)
                            used = True
                            self.update_command_list(msg)

                    # Evaluate if the intend was to ask for a command or just using the syntax
                    if msg_check:
                        variable_dict = self.user_arguments(username[0], channel[0], msg, msg_breakup)
                        if variable_dict:
                            self.return_user_functions(msg_breakup)(**variable_dict)
                            used = True
                            self.update_command_list(msg)

                    if not used and msg_check and not msg:
                        self.chan_or_private_message(username[0], "Command Not Found, "
                                                                  "are you sure this is a valid command?", channel[0])
                except TypeError:
                    self.chan_or_private_message(username[0], "Command Not Found, "
                                                              "are you sure this is a valid command?", channel[0])
                except IndexError:
                    self.chan_or_private_message(username[0], "Command present, but usage incorrect. "
                                                              "Check help docs and try again.", channel[0])

            except queue.Empty:
                pass
